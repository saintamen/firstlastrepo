package TypyIntISystemOutPrintln.Zadanie5;

public class PrzykladIf {
    public static void main(String[] args) {
        // W zadaniu manipuluj liczbą

        int liczba = 3;
        if (liczba < 1) {
            System.out.println("Przykład 1: Liczba mniejsza od 1");
        }
        if (liczba < 2) {
            System.out.println("Przykład 1: Liczba mniejsza od 2");
        }
        if (liczba < 3) {
            System.out.println("Przykład 1: Liczba mniejsza od 3");
        }
        if (liczba < 4) {
            System.out.println("Przykład 1: Liczba mniejsza od 4");
        }
        if (liczba < 5) {
            System.out.println("Przykład 1: Liczba mniejsza od 5");
        }
        if (liczba < 6) {
            System.out.println("Przykład 1: Liczba mniejsza od 6");
        }
        if (liczba < 7) {
            System.out.println("Przykład 1: Liczba mniejsza od 7");
        }

        if (liczba < 1) {
            System.out.println("Przykład 2: Liczba mniejsza od 1");
        } else if (liczba < 2) {
            System.out.println("Przykład 2: Liczba mniejsza od 2");
        } else if (liczba < 3) {
            System.out.println("Przykład 2: Liczba mniejsza od 3");
        } else if (liczba < 4) {
            System.out.println("Przykład 2: Liczba mniejsza od 4");
        } else if (liczba < 5) {
            System.out.println("Przykład 2: Liczba mniejsza od 5");
        } else if (liczba < 6) {
            System.out.println("Przykład 2: Liczba mniejsza od 6");
        } else if (liczba < 7) {
            System.out.println("Przykład 2: Liczba mniejsza od 7");
        }
    }
}
