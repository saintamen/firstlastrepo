package TypyIntISystemOutPrintln.Zadanie4;

public class MainZadanie4 {
    public static void main(String[] args) {
        /*
        Zadanie 4: Zadeklaruj zmienne logiczne (boolean) które noszą nazwe:
                jest_cieplo
                wieje_wiatr
                swieci_slonce

                oraz zmienne:
                ubieram_sie_cieplo - jesli nie jest cieplo lub wieje wiatr
                biore_parasol - jesli nie swieci slonce i nie wieje wiatr
                ubieram_kurtke - jesli wieje, nie ma slonca i nie jest cieplo
         */
        boolean jest_cieplo = false;
        boolean wieje_wiatr = false;
        boolean swieci_slonce = true;

        boolean ubieram_sie_cieplo = !jest_cieplo || wieje_wiatr ;
        boolean biore_parasol = !swieci_slonce && !wieje_wiatr;

        boolean ubieram_kurtke = wieje_wiatr && !swieci_slonce && !jest_cieplo;
        System.out.println("Biore parasol: "+ ubieram_sie_cieplo + "Ubieram kurtke: " + ubieram_kurtke + "Ubieram sie cieplo: "+ ubieram_sie_cieplo);


    }
}
