package TypyIntISystemOutPrintln.Zadanie4;

public class MainZadanie4Przyklad {
    public static void main(String[] args) {

        int zmienna_liczbowa = 1;
        boolean zmienna_1 = true;
        boolean zmienna_2 = false;

        // AND - jesli oba mają wartość true
        boolean wynik = zmienna_1 && zmienna_2;
        System.out.println(wynik);

        // OR - jesli ktorykolwiek z nich jest true
        boolean wynik2 = zmienna_1 || zmienna_2;
        System.out.println(wynik2);

        // EQUAL - sprawdzenie czy są sobie równe
        boolean wynik3 = (zmienna_1 == zmienna_2);
        System.out.println(wynik3);

        /////--------------
        // NIE MYLIC Z PRZYPISANIEM (jedno = ):
//        boolean wynik3 = zmienna_1 = zmienna_2;

        // równoważn`e

//        zmienna_1 = zmienna_2;
//        boolean wynik3 = zmienna_1;
        /////--------------
        // NOT EQUAL - sprawdzenie czy są sobie nierówne
        boolean wynik4 = (zmienna_1 != zmienna_2);
        System.out.println(wynik4);

    }
}
