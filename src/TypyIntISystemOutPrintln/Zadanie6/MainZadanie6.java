package TypyIntISystemOutPrintln.Zadanie6;

public class MainZadanie6 {
    public static void main(String[] args) {
        int ocena_matematyka = 5;
        int ocena_chemia = 2;
        int ocena_j_polski = 1;
        int ocena_j_angielski = 3;
        int ocena_wos = 3;
        int ocena_informatyka = 5;

        // zmienna float -
        // zmienna double -
        double czesc_scisla = (ocena_informatyka + ocena_matematyka + ocena_chemia) / 3.0;
        double czesc_humanistyczna = (ocena_j_angielski + ocena_j_polski + ocena_wos) / 3.0;
        double srednia_ogolna = (ocena_informatyka + ocena_matematyka + ocena_chemia + ocena_j_angielski + ocena_j_polski + ocena_wos) / 6.0;

        System.out.println("Scisla: " +czesc_scisla);
        System.out.println("Human: " +czesc_humanistyczna);
        System.out.println("Ogolna: " +srednia_ogolna);

        if(ocena_chemia == 1){
            System.out.println("Ocena z chemii jest niedostateczna");
        }
        if(ocena_j_angielski == 1){
            System.out.println("Ocena z ang jest niedostateczna");
        }
        if(ocena_informatyka== 1){
            System.out.println("Ocena z informatyki jest niedostateczna");
        }
        if(ocena_j_polski== 1){
            System.out.println("Ocena z polskiego jest niedostateczna");
        }
        if(ocena_matematyka== 1){
            System.out.println("Ocena z matematyki jest niedostateczna");
        }
        if(ocena_wos== 1){
            System.out.println("Ocena z wosu jest niedostateczna");
        }
    }
}
