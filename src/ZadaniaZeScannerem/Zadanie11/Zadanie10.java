package ZadaniaZeScannerem.Zadanie11;

import java.util.Scanner;

public class Zadanie10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę:");
        double pierwsza_liczba = sc.nextDouble();
        System.out.println("Podaj działanie:");
        String dzialanie = sc.next();
        System.out.println("Podaj drugą liczbę:");
        double druga_liczba = sc.nextDouble();

//        if (dzialanie.equals("+")) {
//            double wynik = pierwsza_liczba + druga_liczba;
//            System.out.println("Wynik = " + wynik);
//        } else if (dzialanie.equals("-")) {
//            double wynik = pierwsza_liczba - druga_liczba;
//            System.out.println("Wynik = " + wynik);
//        } else if (dzialanie.equals("*")) {
//            double wynik = pierwsza_liczba * druga_liczba;
//            System.out.println("Wynik = " + wynik);
//        } else if (dzialanie.equals("/")) {
//            if (druga_liczba == 0) {
//                System.out.println("Nie można wykonać działania.");
//            } else {
//                double wynik = pierwsza_liczba / druga_liczba;
//                System.out.println("Wynik = " + wynik);
//            }
//        }

        double wynik =0;

        if (dzialanie.equals("+")) {
            wynik = pierwsza_liczba + druga_liczba;
        } else if (dzialanie.equals("-")) {
            wynik = pierwsza_liczba - druga_liczba;
        } else if (dzialanie.equals("*")) {
            wynik = pierwsza_liczba * druga_liczba;
        } else if (dzialanie.equals("/")) {
            if (druga_liczba == 0) {
                System.out.println("Nie można wykonać działania.");
            } else {
                wynik = pierwsza_liczba / druga_liczba;
            }
        }
        System.out.println("Wynik = " + wynik);
    }
}
