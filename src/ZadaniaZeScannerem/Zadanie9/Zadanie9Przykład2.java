package ZadaniaZeScannerem.Zadanie9;

import java.util.Scanner;

public class Zadanie9Przykład2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        double a = sc.nextDouble();
        System.out.println("Podaj operację do wykonania: ");
        String operacja = sc.next();

        System.out.println("Podaj liczbę: ");
        double b = sc.nextDouble();
        double wynik;

        if (!operacja.equals("/") || !operacja.equals("*") ||
                !operacja.equals("+") || !operacja.equals("-")) {
            System.out.println("niepoprawna operacja");
        }

        if (operacja.equals("*")) {
            wynik = a * b;
            System.out.println("Wynik :" + wynik);
        } else if (operacja.equals("/")) {
            wynik = a / b;
            System.out.println("Wynik :" + wynik);
        } else if (operacja.equals("+")) {
            wynik = a + b;
            System.out.println("Wynik :" + wynik);
        } else if (operacja.equals("-")) {
            wynik = a - b;
            System.out.println("Wynik :" + wynik);
        } else System.out.println("Podaj jedną z dozwolonych operacji *, /, +, -");
    }
}
