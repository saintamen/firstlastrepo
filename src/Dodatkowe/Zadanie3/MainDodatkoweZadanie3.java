package Dodatkowe.Zadanie3;

import java.util.Scanner;

public class MainDodatkoweZadanie3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int rozmiar = sc.nextInt();

        int srodek = rozmiar / 2;
        int promien_do_kwadratu = srodek * srodek;
        for (int x = 0; x < rozmiar; x++) {
            for (int y = 0; y < rozmiar; y++) {
                int x_od_srodka = srodek - x;
                int y_od_srodka = srodek - y;
                int suma_kwadratwow = (x_od_srodka * x_od_srodka) + (y_od_srodka * y_od_srodka);

                int roznica = suma_kwadratwow - promien_do_kwadratu;
                if (roznica <=1) {
                    System.out.print(".");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
