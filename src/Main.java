public class Main {
    public static void main(String[] args) {
        int zmienna_liczbowa = 1;
        boolean zmienna_1 = true;
        boolean zmienna_2 = false;

        boolean wynik = zmienna_1 && zmienna_2;
        System.out.println(wynik);

        boolean wynik2 = zmienna_1 || zmienna_2;
        System.out.println(wynik2);

//        boolean wynik3 = zmienna_1 = zmienna_2;

        // równoważne

//        zmienna_1 = zmienna_2;
//        boolean wynik3 = zmienna_1;
        boolean wynik3 = (zmienna_1 == zmienna_2);
        System.out.println(wynik3);

        boolean wynik4 = (zmienna_1 != zmienna_2);
        System.out.println(wynik4);

    }
}
