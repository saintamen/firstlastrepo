package TabliceIPetle.Zadanie17;

import java.util.Scanner;

public class MainZadanie17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe:");
        int liczba = sc.nextInt();

//        for (int sprawdzana = 1; sprawdzana < liczba; sprawdzana = sprawdzana * 2) {
//        for (int sprawdzana = 1; sprawdzana < liczba; sprawdzana *= 2) {
//            System.out.println(sprawdzana);
//        }
//
//        int sprawdzana = 1;
//        while(sprawdzana < liczba){
//            System.out.println(sprawdzana);
//            sprawdzana*=2;
//        }
//

        int sprawdzana = 1;
        while(Math.pow(2, sprawdzana)< liczba){
            System.out.println(Math.pow(2, sprawdzana));
            sprawdzana+=1;
        }
    }
}
