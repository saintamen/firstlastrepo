package TabliceIPetle.Zadanie22;

import java.util.Scanner;

public class MainZadanie22 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();

//        int dzielnik = 1;
//        while (liczba >= dzielnik) {
//            if (liczba % dzielnik == 0) {
//                System.out.println(dzielnik);
//            }
//            dzielnik++;`
//        }

        for (int dzielnik = 1; liczba >= dzielnik; dzielnik++) {
            if (liczba % dzielnik == 0) {
                System.out.println(dzielnik);
            }
        }
    }
}
