package TabliceIPetle.Zadanie21;

import java.util.Scanner;

public class MainZadanie21 {
    public static void main(String[] args) {

        int rozmiar = 5;

        for (int y = 0; y < rozmiar ; y++) {

            for (int x = rozmiar; x >= 0; x--) {
                if (x < y) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            for (int x = 0; x < rozmiar * 2; x++) {
                if (x > y) {
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
