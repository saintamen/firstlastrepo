package TabliceIPetle.Zadanie24;

import java.util.Random;

public class MainZadanie24 {
    public static void main(String[] args) {
        int[] tablica = new int[10];

        Random generator = new Random();

        // wygenerowanie liczb do tablicy
        for (int i = 0; i < 10; i++) {
            tablica[i] = generator.nextInt(21) - 10;
        }

        // wypisanie tablicy
        for (int i = 0; i < 10; i++) {
            System.out.println("Wartość w komórce: " + i + " to: " + tablica[i]);
        }

        // minimum i maximum
        int min = tablica[0];
        int max = tablica[0];
        for (int i = 0; i < 10; i++) {
            if (min > tablica[i]) {
                min = tablica[i];
            }
            if (max < tablica[i]) {
                max = tablica[i];
            }
        }

        System.out.println("Najwiekszy element to:" + max);
        System.out.println("Najmniejszy element to:" + min);

        // obliczanie sredniej
        double suma = 0;
        for (int i = 0; i < 10; i++) {
            suma += tablica[i];
        }

        double srednia = suma / tablica.length;
        System.out.println("Srednia wynosi: " + srednia);

        // wieksze / mniejsze
        int wieksze = 0;
        int mniejsze = 0;

        for (int i = 0; i < 10; i++) {
            if (tablica[i] > srednia) {
                wieksze++;
            }
            if (tablica[i] < srednia) {
                mniejsze++;
            }
        }

        System.out.println("Jest " + wieksze + " wiekszych elementów");
        System.out.println("Jest " + mniejsze + " mniejszych elementów");

        // odwrotna kolejność
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.println("Element o numerze " + i + " wartość: " + tablica[i]);
        }

        // inny sposob na to samo
        for (int i = 0; i < 10; i++) {
            System.out.println("Element o numerze " + (9 - i) + " wartość: " + tablica[9- i]);
        }
    }
}
